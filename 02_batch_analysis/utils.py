import numpy as np
import bigfish.stack as stack
import bigfish.detection as detection
from skimage.filters import gaussian, threshold_isodata
from scipy.ndimage import binary_fill_holes

# Definition of extra functions

# Read images
def read_images(nuc_path, rna_path):
    nuc = stack.read_image(nuc_path)
    print("dapi channel")
    print("\r shape: {0}".format(nuc.shape))
    print("\r dtype: {0}".format(nuc.dtype), "\n")

    rna = stack.read_image(rna_path)
    print("fish channel")
    print("\r shape: {0}".format(rna.shape))
    print("\r dtype: {0}".format(rna.dtype))

    return nuc, rna

# Create mask to eliminate spots outside cells
def create_rna_mask(rna, sigma, hole_size):
    rna_blur = gaussian(rna, sigma=sigma)
    rna_mask_list = []
    for rna_slice in rna_blur:
        threshold_value = threshold_isodata(rna_slice)
        rna_slice_binary = binary_fill_holes(rna_slice > threshold_value)
        rna_mask_list.append(rna_slice_binary)

    return np.uint8(np.array(rna_mask_list))

# Filter detected spots using mask
def filter_spots(spots, mask):
    filter = np.bool8(mask[spots[:, 0], spots[:, 1], spots[:, 2]])
    filtered_spots = spots[filter]
    print('After filtering')
    print("\r shape: {0}".format(filtered_spots.shape))
    return filtered_spots
    
# Detect spots using bigfish
def detect_spots(rna, voxel_size, blob_size, threshold):

    # spot radius
    spot_radius_px = detection.get_object_radius_pixel(
        voxel_size_nm = voxel_size,  # in nanometer (one value per dimension zyx)
        object_radius_nm = blob_size,  # in nanometer (one value per dimension zyx)
        ndim=3)

    # LoG filter
    rna_log = stack.log_filter(rna, sigma=spot_radius_px)

    # local maximum detection
    mask = detection.local_maximum_detection(rna_log, min_distance=spot_radius_px)

    spots, _ = detection.spots_thresholding(rna_log, mask, threshold)
    print("detected spots")
    print("\r shape: {0}".format(spots.shape))
    print("\r threshold: {0}".format(threshold))

    return spots
