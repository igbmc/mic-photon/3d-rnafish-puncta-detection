# Puncta detection and spatial distribution analysis

## Goal

1. Detect and count RNA-FISH spots
2. Analyze their spatial distribution in the stem cell colony. Estimate nuclear/cytoplasm ratio of spots distribution.

## Analysis pipeline

0. Segment nuclear channel to obtain 3D instance segmentation of each nucleus
1. Use big-fish to detect RNA-FISH spots with determined parameters
2. Process all files with same parameters and export all found spots
3. Use an approximative cell mask and the segmented nuclear mask to identify cytoplasmic and nuclear spots. Then calculate the ratio between the two.

## Author
Marco Dalla Vecchia dallavem@igbmc.fr