# %% [markdown]
# **In case of problems or questions, please first check the list of [Frequently Asked Questions (FAQ)](https://stardist.net/docs/faq.html).**
# 
# Please shutdown all other training/prediction notebooks before running this notebook (as those might occupy the GPU memory otherwise).

# %%
from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from glob import glob
import os
from tqdm import tqdm
from tifffile import imread
from csbdeep.utils import Path, normalize

from stardist import fill_label_holes, random_label_cmap, calculate_extents, gputools_available
from stardist import Rays_GoldenSpiral
from stardist.matching import matching, matching_dataset
from stardist.models import Config3D, StarDist3D, StarDistData3D

from skimage.transform import resize

# %% [markdown]
# # Data
# 
# We assume that data has already been downloaded via notebook [1_data.ipynb](1_data.ipynb).  
# 
# <div class="alert alert-block alert-info">
# Training data (for input `X` with associated label masks `Y`) can be provided via lists of numpy arrays, where each image can have a different size. Alternatively, a single numpy array can also be used if all images have the same size.  
# Input images can either be three-dimensional (single-channel) or four-dimensional (multi-channel) arrays, where the channel axis comes last. Label images need to be integer-valued.
# </div>

# %%
img_path = '../../../data/'
label_path = '../../../processing/Output/Segmentation/nuclei/'

img_paths = sorted([path for path in glob(img_path + '*H4/*DAPI.TIF') if "SR" not in path if "neg" not in path if "Temp" not in path if "LiveSr" not in path])
label_paths = sorted(glob(label_path + '*'))

assert len(label_paths) == len(img_paths), "Images and labels don't correspond!"

# %%
for idx in range(len(img_paths)):
    print(os.path.basename(img_paths[idx]), '-->' ,os.path.basename(img_paths[idx]))

    
resizing_factor = 4

X, Y = [], []
for img, label in zip(img_paths, label_paths):
    temp = imread(img)
    resized_temp = resize(temp, (temp.shape[0], temp.shape[1] / resizing_factor, temp.shape[2] / resizing_factor), anti_aliasing=True, preserve_range=True, mode='constant')
    X.append(resized_temp)
    
    temp = imread(label)
    resized_temp = resize(temp, (temp.shape[0], temp.shape[1] / resizing_factor, temp.shape[2] / resizing_factor), anti_aliasing=False, preserve_range=False, order=0, mode='constant').astype('uint16')
    Y.append(resized_temp)
    
print(len(X), len(Y))

# %%
# Visualize resizing
# f, (ax1, ax2) = plt.subplots(1,2, figsize=(16,10))
# ax1.imshow(temp[int(temp.shape[0] / 2)])
# ax1.set_title(temp.shape)
# ax2.imshow(Y[-1][int(Y[-1].shape[0] / 2)])
# ax2.set_title(Y[-1].shape)
# plt.show()

# %% [markdown]
# Normalize images and fill small label holes.

# %%
axis_norm = (0,1,2)   # normalize channels independently
# axis_norm = (0,1,2,3) # normalize channels jointly
# if n_channel > 1:
#     print("Normalizing image channels %s." % ('jointly' if axis_norm is None or 3 in axis_norm else 'independently'))
#     sys.stdout.flush()

X = [normalize(x,1,99.8,axis=axis_norm) for x in tqdm(X)]
Y = [fill_label_holes(y) for y in tqdm(Y)]

# %% [markdown]
# Split into train and validation datasets.

# %%
assert len(X) > 1, "not enough training data"
rng = np.random.RandomState(42)
ind = rng.permutation(len(X))
n_val = max(1, int(round(0.15 * len(ind))))
ind_train, ind_val = ind[:-n_val], ind[-n_val:]
X_val, Y_val = [X[i] for i in ind_val]  , [Y[i] for i in ind_val]
X_trn, Y_trn = [X[i] for i in ind_train], [Y[i] for i in ind_train] 
print('number of images: %3d' % len(X))
print('- training:       %3d' % len(X_trn))
print('- validation:     %3d' % len(X_val))

# %% [markdown]
# Training data consists of pairs of input image and label instances.

# %%
def plot_img_label(img, lbl, img_title="image (XY slice)", lbl_title="label (XY slice)", z=None, **kwargs):
    if z is None:
        z = img.shape[0] // 2    
    fig, (ai,al) = plt.subplots(1,2, figsize=(12,5), gridspec_kw=dict(width_ratios=(1.25,1)))
    im = ai.imshow(img[z], cmap='gray', clim=(0,1))
    ai.set_title(img_title)    
    fig.colorbar(im, ax=ai)
    al.imshow(lbl[z])
    al.set_title(lbl_title)
    plt.tight_layout()

# %%
# i = 0
# img, lbl = X[i], Y[i]
# assert img.ndim in (3,4)
# img = img if img.ndim==3 else img[...,:3]
# plot_img_label(img,lbl)
# None;

# %%
extents = calculate_extents(Y)
anisotropy = tuple(np.max(extents) / extents)
print('empirical anisotropy of labeled objects = %s' % str(anisotropy))

# %%
# 96 is a good default choice (see 1_data.ipynb)
n_rays = 128

# Use OpenCL-based computations for data generator during training (requires 'gputools')
use_gpu = True

# Predict on subsampled grid for increased efficiency and larger field of view
# grid = tuple(1 if a > 1.5 else 2 for a in anisotropy)
grid = (1,4,4)
n_channel = 1

# Use rays on a Fibonacci lattice adjusted for measured anisotropy of the training data
rays = Rays_GoldenSpiral(n_rays, anisotropy=anisotropy)

conf = Config3D (
    rays             = rays,
    grid             = grid,
    anisotropy       = anisotropy,
    use_gpu          = use_gpu,
    n_channel_in     = n_channel,
    # adjust for your data below (make patch size as large as possible)
    train_patch_size = (32,128,128),
    train_batch_size = 2,
    train_epochs = 1000,
)
print(conf)
vars(conf)

# %%
if use_gpu:
    from csbdeep.utils.tf import limit_gpu_memory
    # adjust as necessary: limit GPU memory to be used by TensorFlow to leave some to OpenCL-based computations
    # limit_gpu_memory(0.8)
    # alternatively, try this:
    limit_gpu_memory(None, allow_growth=True)

# %% [markdown]
# **Note:** The trained `StarDist3D` model will *not* predict completed shapes for partially visible objects at the image boundary.

# %%
model = StarDist3D(conf, name='stardist_faezeh', basedir='models')

# %% [markdown]
# Check if the neural network has a large enough field of view to see up to the boundary of most objects.

# %%
median_size = calculate_extents(Y, np.median)
fov = np.array(model._axes_tile_overlap('ZYX'))
print(f"median object size:      {median_size}")
print(f"network field of view :  {fov}")
if any(median_size > fov):
    print("WARNING: median object size larger than field of view of the neural network.")

# %% [markdown]
# # Data Augmentation

# %% [markdown]
# You can define a function/callable that applies augmentation to each batch of the data generator.  
# We here use an `augmenter` that applies random rotations, flips, and intensity changes, which are typically sensible for (3D) microscopy images (but you can disable augmentation by setting `augmenter = None`).

# %%
def random_fliprot(img, mask, axis=None): 
    if axis is None:
        axis = tuple(range(mask.ndim))
    axis = tuple(axis)
            
    assert img.ndim>=mask.ndim
    perm = tuple(np.random.permutation(axis))
    transpose_axis = np.arange(mask.ndim)
    for a, p in zip(axis, perm):
        transpose_axis[a] = p
    transpose_axis = tuple(transpose_axis)
    img = img.transpose(transpose_axis + tuple(range(mask.ndim, img.ndim))) 
    mask = mask.transpose(transpose_axis) 
    for ax in axis: 
        if np.random.rand() > 0.5:
            img = np.flip(img, axis=ax)
            mask = np.flip(mask, axis=ax)
    return img, mask 

def random_intensity_change(img):
    img = img*np.random.uniform(0.6,2) + np.random.uniform(-0.2,0.2)
    return img

def augmenter(x, y):
    """Augmentation of a single input/label image pair.
    x is an input image
    y is the corresponding ground-truth label image
    """
    # Note that we only use fliprots along axis=(1,2), i.e. the yx axis 
    # as 3D microscopy acquisitions are usually not axially symmetric
    x, y = random_fliprot(x, y, axis=(1,2))
    x = random_intensity_change(x)
    return x, y


# %%
# plot some augmented examples
# img, lbl = X[0],Y[0]
# plot_img_label(img, lbl)
# for _ in range(3):
#     img_aug, lbl_aug = augmenter(img,lbl)
#     plot_img_label(img_aug, lbl_aug, img_title="image augmented (XY slice)", lbl_title="label augmented (XY slice)")

# %% [markdown]
# # Training

# %% [markdown]
# We recommend to monitor the progress during training with [TensorBoard](https://www.tensorflow.org/programmers_guide/summaries_and_tensorboard). You can start it in the shell from the current working directory like this:
# 
#     $ tensorboard --logdir=.
# 
# Then connect to [http://localhost:6006/](http://localhost:6006/) with your browser.
# 

# %%
model.train(X_trn, Y_trn, validation_data=(X_val,Y_val), augmenter=augmenter)

# %% [markdown]
# # Threshold optimization

# %% [markdown]
# While the default values for the probability and non-maximum suppression thresholds already yield good results in many cases, we still recommend to adapt the thresholds to your data. The optimized threshold values are saved to disk and will be automatically loaded with the model.

# %%
model.optimize_thresholds(X_val, Y_val)
